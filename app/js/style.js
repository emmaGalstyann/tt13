$(document).ready(function() {
  if ($(window).width() > 1024) {
    var swiper = $('.swiper-container').swiper({
      pagination: '.swiper-pagination',
      paginationClickable: true,
      paginationBulletRender: function(index, className) {
        return (
          '<div class="index' +
          index +
          ' ' +
          className +
          '"><div class="named"></div><div><span class="bullet"></span></div></div>'
        );
      },
      direction: 'vertical',
      slidesPerView: 1,
      paginationClickable: true,
      spaceBetween: 0,
      mousewheelControl: true,
      mousewheelForceToAxis: true,
      speed: 300,
      effect: 'fade',
      onInit: function() {
        $('.swiper-pagination > div:nth-child(1)')
          .find('.named')
          .append('<p><span>01</span><span>главная</span></p>');
        $('.swiper-pagination > div:nth-child(2)')
          .find('.named')
          .append('<p><span>02</span><span>про кошелёк</span></p>');
        $('.swiper-pagination > div:nth-child(3)')
          .find('.named')
          .append('<p><span>03</span><span>преимущества</span></p>');
        $('.swiper-pagination > div:nth-child(12)')
          .find('.named')
          .append('<p><span>04</span>перспективы</p>');
      },
      onSlideChangeEnd: function() {
        var el = $('.swiper-pagination-bullet-active');
        if (el.hasClass('index2') || el.hasClass('index10')) {
          el.parent().addClass('drob');
          el.prevAll().removeClass('last');
        } else if (
          el.hasClass('index0') ||
          el.hasClass('index1') ||
          el.hasClass('index11')
        ) {
          el.parent().removeClass('drob');
          el.prevAll().addClass('last');
        }
        el.prevAll()
          .addClass('prev')
          .removeClass('active');

        el.removeClass('prev').addClass('active');

        el.nextAll().removeClass('prev active');
      },
    });
  }
});

var translate = {
  '#eng': {
    lang: 'Eng',
    'menu-one': 'about wallet',
  },
  '#rus': {
    lang: 'Pyc',
    'menu-one': 'Про кошелёк',
  },
};
var langChange = function(lang) {
  if (lang !== '') {
    $('.language-change').each(function() {
      $(this).text(translate[lang][$(this).attr('data-language')]);
      $('input.language-change').each(function() {
        $(this).attr(
          'placeholder',
          translate[lang][$(this).attr('data-language')]
        );
      });
    });
  }
};

var url_string = window.location.hash;
langChange(url_string);

$(document).ready(function() {
  $('.lang ul').removeClass('open');

  /*меняю цвет */
  $('.trigger').on('click', function() {
    $('body').toggleClass('light dark');
  });

  /*работа с попапом */
  if ($(window).width() > 1024) {
    $('.pop-up-open').on('click', function(e) {
      e.preventDefault();
      var currentModal = $(this).attr('href');
      $('.pop-up-wrapper').each(function() {
        var currentId = '#' + $(this).attr('id');
        if (currentId === currentModal) {
          $(this).addClass('open');
          $('body').addClass('overflow');
        }
      });
    });
    $('.pop-up .close').on('click', function() {
      $('.pop-up-wrapper').removeClass('open');
      $('body').removeClass('overflow');
    });
    $('.pop-up-wrapper').on('click touchstart', function(event) {
      var element = $('.pop-up');
      if (!$(event.target).closest(element).length) {
        $('.pop-up-wrapper').removeClass('open');
        $('body').removeClass('overflow');
      }
    });
  } else {
    $('.pop-up-open').on('click', function(e) {
      e.preventDefault();
      var currentModal = $(this).attr('href');
      $('.pop-up-wrapper').each(function() {
        var currentId = '#' + $(this).attr('id');
        if (currentId === currentModal) {
          $('.menu-burger').addClass('pop-up-open active');
          $('.lang').removeClass('active');
          $('.fixed-btn').removeClass('open');
          $('body').addClass('overflow');
          if ($(this).attr('id') == 'message-pop-up') {
            $(this).addClass('open');
            $('.pop-up-wrapper#email-pop-up').removeClass('open');
          } else {
            $(this).addClass('open');
          }
        }
      });
    });

    $('.menu-burger').on('click', function() {
      if ($(this).hasClass('pop-up-open')) {
        $('.menu-burger').removeClass('pop-up-open active');
        $('.pop-up-wrapper').removeClass('open');
        $('nav').removeClass('open');
        $('header').removeClass('open');
        $('.lang').removeClass('active');
        $('.fixed-btn').removeClass('open');
        $('body').removeClass('overflow');
      } else if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('header').removeClass('open');
        $('nav').removeClass('open');
        $('.lang').removeClass('active');
        $('body').removeClass('overflow');
        $('.fixed-btn').removeClass('open');
      } else {
        $(this).addClass('active');
        $('header').addClass('open');
        $('nav').addClass('open');
        $('.lang').removeClass('active');
        $('.fixed-btn').addClass('open');
        $('body').addClass('overflow');
      }
    });
  }

  /*изменение языка */

  $('.lang').on('click', function() {
    $('.lang').toggleClass('active');
  });

  $('.lang ul li a').on('click', function() {
    var lang = $(this).attr('href');
    langChange(lang);
  });

  /* form send */
  $('#messageForm').each(function() {
    var thisForm = $(this);
    thisForm.validate({
      rules: {
        email: {
          email: true,
          required: true,
        },
      },
      submitHandler: function(form) {
        var data = {};
        thisForm.find('label').each(function() {
          var thisEl = $(this).children();
          data[thisEl.attr('name')] = thisEl.val();
        });
        console.log(data);
        $.ajax({
          type: 'POST', //Метод отправки
          url: '../../sendMessage.php', //путь до php фаила отправителя
          data: data,
          success: function() {
            console.log(data);
            //код в этом блоке выполняется при успешной отправке сообщения
            alert('Ваше сообщение отправлено!');
            $('#messageForm').trigger('reset');
          },
        });
      },
    });
  });
  $('.emailForm').each(function() {
    var thisForm = $(this);
    thisForm.validate({
      rules: {
        email: {
          email: true,
          required: true,
        },
      },
      submitHandler: function(form) {
        var data = {};
        thisForm.find('label').each(function() {
          var thisEl = $(this).children();
          data[thisEl.attr('name')] = thisEl.val();
        });
        console.log(data);
        $.ajax({
          type: 'POST', //Метод отправки
          url: '../../sendEmail.php', //путь до php фаила отправителя
          data: data,
          success: function() {
            console.log(data);
            //код в этом блоке выполняется при успешной отправке сообщения
            alert('Ваше сообщение отправлено!');
            $('.emailForm').trigger('reset');
          },
        });
      },
    });
  });
});
