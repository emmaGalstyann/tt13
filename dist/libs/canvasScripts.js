var canvas;
var gl;
var realToCSSPixels = window.devicePixelRatio;
var displayWidth;
var displayHeight;
var rings;
var createdMetaballs = [];
var assetsIndexToLoad = 0;
var assetsToLoad = [
  {
    path: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-183/',
    src: 'noise3.png',
    name: 'noise3',
    type: 'texture',
  },
];
var assets = {};

window.onload = preloadAssets;

function preloadAssets() {
  function checkIfAllAssetsAreLoaded() {
    if (assetsIndexToLoad < assetsToLoad.length) {
      loadAssetIndex(assetsIndexToLoad);
    } else {
      initialize();
      initialize2();
    }
  }

  function loadAssetIndex(index) {
    var objectToLoad = assetsToLoad[index];

    switch (objectToLoad.type) {
      case 'texture':
        var image = new Image();
        image.onload = function(event) {
          assets[objectToLoad.name] = this;
          assetsIndexToLoad++;
          checkIfAllAssetsAreLoaded();
        };
        image.crossOrigin = '';
        image.src = objectToLoad.path + objectToLoad.src;
        break;
    }
  }

  loadAssetIndex(assetsIndexToLoad);
}

function initialize() {
  canvas = document.getElementById('metaball-canvas');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var glConfig = {
    premultipliedAlpha: true,
    antialias: true,
    depth: true,
    alpha: true,
  };

  gl =
    canvas.getContext('webgl', glConfig) ||
    canvas.getContext('experimental-webgl', glConfig);

  if (!gl) {
    console.error('cannot find gl', gl);
    return;
  }
  displayWidth = Math.floor(gl.canvas.clientWidth / realToCSSPixels);
  displayHeight = Math.floor(gl.canvas.clientHeight / realToCSSPixels);

  var minSpeed = 0.2;
  var maxSpeed = 2.5;
  var minMultiplierArcX = -0.25;
  var maxMultiplierArcX = 0.75;
  var minMultiplierArcY = -0.25;
  var maxMultiplierArcY = 0.25;
  var scale = 1.0;

  var metaballsGroup7 = {
    metaballs: [
      /*две маленькие сверху */
      {
        centerOffsetX: -580 * scale,
        centerOffsetY: 330 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -545 * scale,
        centerOffsetY: 315 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*три слева снизу*/

      {
        centerOffsetX: -970 * scale,
        centerOffsetY: -150 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 150,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -850 * scale,
        centerOffsetY: -250 * scale,
        radius: 60 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 150,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -970 * scale,
        centerOffsetY: -350 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 160,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*две справа снизу*/
      {
        centerOffsetX: 890 * scale,
        centerOffsetY: -250 * scale,
        radius: 60 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 735 * scale,
        centerOffsetY: -280 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*одна снизу*/
      {
        centerOffsetX: -150 * scale,
        centerOffsetY: -275 * scale,
        radius: 50 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*центральные*/
      {
        centerOffsetX: 150 * scale,
        centerOffsetY: 80 * scale,
        radius: 100 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -150 * scale,
        centerOffsetY: 60 * scale,
        radius: 75 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 20 * scale,
        centerOffsetY: -60 * scale,
        radius: 40 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 10 * scale,
        centerOffsetY: -50 * scale,
        radius: 40 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*централная маленькая */

      {
        centerOffsetX: 0 * scale,
        centerOffsetY: 320 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
    ],
    texture: generateGradientTexture(false, false, false),
  };


  createdMetaballs.push(new Metaballs(gl, metaballsGroup7));

  for (var i = 0; i < createdMetaballs.length; i++) {
    setTimeout(createdMetaballs[i].fadeIn, i * 200);
  }
  window.addEventListener('resize', onWindowResize);
  window.addEventListener('mousemove', onWindowMouseMove);

  resizeGL(gl);

  step();
}

function initialize2() {
  canvas = document.getElementById('metaball-canvas2');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var glConfig = {
    premultipliedAlpha: true,
    antialias: true,
    depth: true,
    alpha: true,
  };

  gl =
    canvas.getContext('webgl', glConfig) ||
    canvas.getContext('experimental-webgl', glConfig);

  if (!gl) {
    console.error('cannot find gl', gl);
    return;
  }
  displayWidth = Math.floor(gl.canvas.clientWidth / realToCSSPixels);
  displayHeight = Math.floor(gl.canvas.clientHeight / realToCSSPixels);

  var minSpeed = 0.2;
  var maxSpeed = 2.5;
  var minMultiplierArcX = -0.25;
  var maxMultiplierArcX = 0.75;
  var minMultiplierArcY = -0.25;
  var maxMultiplierArcY = 0.25;
  var scale = 1.0;

  var metaballsGroup7 = {
    metaballs: [
      /*две маленькие сверху */
      {
        centerOffsetX: -580 * scale,
        centerOffsetY: 330 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -545 * scale,
        centerOffsetY: 315 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*три слева снизу*/

      {
        centerOffsetX: -970 * scale,
        centerOffsetY: -150 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 150,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -850 * scale,
        centerOffsetY: -250 * scale,
        radius: 60 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 150,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -970 * scale,
        centerOffsetY: -350 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 160,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*две справа снизу*/
      {
        centerOffsetX: 890 * scale,
        centerOffsetY: -250 * scale,
        radius: 60 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 735 * scale,
        centerOffsetY: -280 * scale,
        radius: 70 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*одна снизу*/
      {
        centerOffsetX: -150 * scale,
        centerOffsetY: -275 * scale,
        radius: 50 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*центральные*/
      {
        centerOffsetX: 150 * scale,
        centerOffsetY: 80 * scale,
        radius: 100 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: -150 * scale,
        centerOffsetY: 60 * scale,
        radius: 75 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 20 * scale,
        centerOffsetY: -60 * scale,
        radius: 40 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
      {
        centerOffsetX: 10 * scale,
        centerOffsetY: -50 * scale,
        radius: 40 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },

      /*централная маленькая */

      {
        centerOffsetX: 0 * scale,
        centerOffsetY: 320 * scale,
        radius: 20 * scale,
        speed: getRandomFloat(minSpeed, maxSpeed),
        t: Math.random() * 200,
        arcMultiplierX: getRandomFloat(minMultiplierArcX, maxMultiplierArcX),
        arcMultiplierY: getRandomFloat(minMultiplierArcY, maxMultiplierArcY),
      },
    ],
    texture: generateGradientTexture2(false, false, false),
  };

  createdMetaballs.push(new Metaballs(gl, metaballsGroup7));

  for (var i = 0; i < createdMetaballs.length; i++) {
    setTimeout(createdMetaballs[i].fadeIn, i * 200);
  }
  window.addEventListener('resize', onWindowResize);
  window.addEventListener('mousemove', onWindowMouseMove);

  resizeGL(gl);

  step();
}

function generateGradientTexture(colors, vertical, debug) {

    colors = [
      { color: '#496DA1', stop: 0.0 },
      { color: '#496DA1', stop: 0.4 },
      { color: '#5498B6', stop: 0.5 },
      { color: '#58A7BD', stop: 1.0 },
    ];

  vertical = vertical !== undefined ? vertical : false;

  var size = 512;

  // create canvas
  var textureCanvas = document.createElement('canvas');
  textureCanvas.width = size;
  textureCanvas.height = size;

  if (debug == true) {
    textureCanvas.style.position = 'absolute';
    textureCanvas.style.top = '0px';
    textureCanvas.style.left = '0px';
    document.body.appendChild(textureCanvas);
  }

  // get context
  var context = textureCanvas.getContext('2d');

  // draw gradient
  context.rect(0, 0, size, size);

  var grd = vertical
    ? context.createLinearGradient(0, size, 0, 0)
    : context.createLinearGradient(0, 0, size, 0);
  for (var i = 0; i < colors.length; i++) {
    grd.addColorStop(colors[i].stop, colors[i].color);
  }
  context.fillStyle = grd;
  context.fillRect(0, 0, size, size);

  return textureCanvas;
}

function generateGradientTexture2(colors, vertical, debug) {
    colors = [
      { color: '#0B4372', stop: 0.0 },
      { color: '#0B4372', stop: 0.4 },
      { color: '#124978', stop: 0.5 },
      { color: '#194F7F', stop: 1.0 },
    ];
  vertical = vertical !== undefined ? vertical : false;

  var size = 512;

  // create canvas
  var textureCanvas = document.createElement('canvas');
  textureCanvas.width = size;
  textureCanvas.height = size;

  if (debug == true) {
    textureCanvas.style.position = 'absolute';
    textureCanvas.style.top = '0px';
    textureCanvas.style.left = '0px';
    document.body.appendChild(textureCanvas);
  }

  // get context
  var context = textureCanvas.getContext('2d');

  // draw gradient
  context.rect(0, 0, size, size);

  var grd = vertical
    ? context.createLinearGradient(0, size, 0, 0)
    : context.createLinearGradient(0, 0, size, 0);
  for (var i = 0; i < colors.length; i++) {
    grd.addColorStop(colors[i].stop, colors[i].color);
  }
  context.fillStyle = grd;
  context.fillRect(0, 0, size, size);

  return textureCanvas;
}

function getRandomFloat(min, max) {
  return Math.random() * (max - min) + min;
}

function onWindowResize(event) {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;

  resizeGL(gl);
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
}

function onWindowMouseMove(event) {
  createdMetaballs.forEach(function(metaball) {
    metaball.handleMouseMove(event.clientX, event.clientY);
  });
}

function resizeGL(gl) {
  realToCSSPixels = window.devicePixelRatio;

  // Lookup the size the browser is displaying the canvas in CSS pixels
  // and compute a size needed to make our drawingbuffer match it in
  // device pixels.
  displayWidth = Math.floor(gl.canvas.clientWidth /** realToCSSPixels*/);
  displayHeight = Math.floor(gl.canvas.clientHeight /** realToCSSPixels*/);

  // Check if the canvas is not the same size.
  if (gl.canvas.width !== displayWidth || gl.canvas.height !== displayHeight) {
    // Make the canvas the same size
    gl.canvas.width = displayWidth;
    gl.canvas.height = displayHeight;
  }

  gl.viewport(0, 0, displayWidth, displayHeight);

  createdMetaballs.forEach(function(metaball) {
    metaball.handleResize(displayWidth, displayHeight);
  });
}

var step = function() {
  createdMetaballs.forEach(function(metaball) {
    metaball.updateSimulation();
  });
  requestAnimationFrame(step);
};